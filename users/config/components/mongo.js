const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

module.exports = {
  bootstrap: () => {
    // const connectionString ='mongodb://host.docker.internal:21017/users';
    // IMPORTANT: host name will be the service name used for mongodb image, in this case it is 'mongo'
    const connectionString ='mongodb://mongo:27017/users';
    mongoose.connect(connectionString, (err, db) => {
      if (err) {
        console.error(err);
        throw new Error('Unable to connect MongoDB');
      }
      console.log(`###### Connected to MongoDB! ###### ${db}`);
      autoIncrement.initialize(db);
    });
    // TODO: Validate DB Address
  },
  config: null,
};
