const mongoose = require('mongoose');

const { Schema } = mongoose;

const user = new Schema({
    name: {
        type: String
    },
    email: {
        type: String,
    },
    phone: {
        type: String,
    }
}, {
    toJSON: {
        virtuals: true,
    },
});

const User = mongoose.model('users', user, 'users');

module.exports = User;
