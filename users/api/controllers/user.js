const user = require('../../api/models/users');

function users(req, res) {
    user.find({}).then((response) => {
        res.json({
            status: 200,
            data: response,
        });
        res.status(200);
    }).catch((err) => {
        console.log(err);
    })
}

module.exports = {
    users,
}