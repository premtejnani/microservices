

function appointments(req, res) {
    res.json({
        appointment_id: 123,
        appointment_place: 'Prems Clinic',
        patient_name: 'Prem',
        patient_id: 456
    });
    res.status(200);
}

module.exports = {
    appointments,
}