# Reference project for microservices

This is a simple reference project that defines the structure of the microservices.

## Getting Started

You can download this project on you local machine. This project contains only two services as of now. You can try creating a service of your own.

### Prerequisites

First you will need to check if virtualization is supported on your machine, run the following command and verify that the output is non-empty:

For Mac Users, use below command:

sysctl -a | grep -E --color 'machdep.cpu.features|VMX'

you should be able to see 'VMX' highlightes in a colour.

For Windows and Linux, find the command in the below doc.

Refer - (https://kubernetes.io/docs/tasks/tools/install-minikube/) 


You will need to have the following things on your local machine.

1. Minikube - Which will provide you a single node kubernetes cluster

2. docker  - Which will let you create docker containers to deploy your service on

For Mac user, first you can start with installing brew , which will really help in easy installation of other things. Refer - (https://docs.brew.sh/Installation)

### Installing

Reference - (https://kubernetes.io/docs/tasks/tools/install-minikube/)

A step by step explanation and commands are provided in the above reference doc.

### Useful Commands

#### Build service
docker-compose build

docker-compose build <service_name> 

#### deploy service/code
docker-compose up

docker-compose up <service_name>     

#### Check for process status (ps)
docker ps

docker ps -a

#### Stop a container
docker stop <container_id>

#### Remove a container
docker rm <container_id_>

#### List Docker images created in your machine
docker images

